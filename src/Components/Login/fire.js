import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyB2n6vGQ4fHe2BrgnNm_aFdVBhifx5Oru8",
  authDomain: "movie-bcbbc.firebaseapp.com",
  projectId: "movie-bcbbc",
  storageBucket: "movie-bcbbc.appspot.com",
  messagingSenderId: "788632446059",
  appId: "1:788632446059:web:6ddacaa0b92dbc1114fd2e",
};

const fire = firebase.initializeApp(firebaseConfig);
export default fire;
