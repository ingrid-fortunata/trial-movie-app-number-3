import React, { useState } from "react";
import Modal from "react-modal";
import "./Modal.css";
import auth_service from "../../services/auth_service";

import movieLogo from "../../assets/movieLogo.png";

export default function SignInModal(props) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  // const [isSignUp, setIsSignUp] = useState(true);
  let err_message = "";
  const {
    email,
    setEmail,
    password,
    setPassword,
    handleLogin,
    handleSignUp,
    hasAccount,
    setHasAccount,
    emailError,
    passwordError,
    setisSignedIn,
  } = props;

  return (
    <div className="signin-modal">
      <h2 onClick={() => setModalIsOpen(true)}>Sign In</h2>
      <Modal isOpen={modalIsOpen} onRequestClose={() => setModalIsOpen(false)}>
        <section className="modal-container">
          <div className="logo">
            <img src={movieLogo} alt="logo" className="movie-logo-header" />
            <h1 className="logo-name">ATV</h1>
          </div>
          <div className="loginContainer">
            <div className="content-item">
              <label className="styleLabel">E-mail</label>
              <input
                type="text"
                autoFocus
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="styleInput"
              />
              <p className="errorMsg">{emailError}</p>
            </div>

            <div className="content-item">
              <label className="styleLabel">Password</label>
              <input
                type="password"
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="styleInput"
              />
              <p className="errorMsg">{passwordError}</p>
            </div>

            <div className="btnContainer">
              {hasAccount ? (
                <>
                  <div className="content-item">
                    <button className="styleButtonSignup" onClick={handleLogin}>
                      Sign In
                    </button>
                  </div>
                  <div className="alreadyhave">
                    <p>Don't have an account? </p>
                    <span onClick={() => setHasAccount(!hasAccount)}>
                      Sign Up
                    </span>
                  </div>
                </>
              ) : (
                <>
                  <div className="content-item">
                    <button
                      className="styleButtonSignup"
                      // onClick={handleSignUp}
                      onClick={() =>
                        (err_message = auth_service.register(
                          email,
                          email,
                          email,
                          password
                        ))
                      }
                    >
                      Sign Up
                    </button>
                    {}
                  </div>
                  <div className="alreadyhave">
                    <p>Have an account? </p>
                    <span onClick={() => setHasAccount(!hasAccount)}>
                      Sign In
                    </span>
                  </div>
                </>
              )}
            </div>
          </div>
        </section>
      </Modal>
    </div>
  );
}
