import React, { useState, useEffect } from "react";
import SignInModal from "../../Modal/SignInModal";
import { Link } from "react-router-dom";
import fire from "../../Login/fire";
import Hero from "../../Login/hero";
import "./header.css";
import actflix from "../../../assets/ACTFLIX.png";

const Header = (props) => {
  // const style = {
  //   color: "#fff",
  // };

  const [user, setUser] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [hasAccount, setHasAccount] = useState(false);

  const clearInput = () => {
    setEmail("");
    setPassword("");
  };

  const clearError = () => {
    setEmailError("");
    setPasswordError("");
  };

  const handleLogin = () => {
    clearError();
    fire
      .auth()
      .signInWithEmailAndPassword(email, password)
      .catch((err) => {
        switch (err.code) {
          case "auth/invalid-email":
          case "auth/user-disabled":
          case "auth/user-not-found":
            setEmailError(err.message);
            break;
          case "auth/wrong-password":
            setPasswordError(err.message);
            break;
        }
      });
  };

  const handleSignUp = () => {
    clearError();
    fire
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .catch((err) => {
        switch (err.code) {
          case "auth/email-already-in-use":
          case "auth/invalid-email":
            setEmailError(err.message);
            break;
          case "auth/weak-password":
            setPasswordError(err.message);
            break;
        }
      });
  };

  const handleSignOut = () => {
    fire.auth().signOut();
  };

  // check if user exist
  const authListener = () => {
    fire.auth().onAuthStateChanged((user) => {
      if (user) {
        clearInput();
        setUser(user);
      } else {
        setUser("");
      }
    });
  };

  useEffect(() => {
    authListener();
  }, []);

  const [show, handleShow] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 100) {
        handleShow(true);
      } else {
        handleShow(false);
      }
    });
    return () => {
      window.removeEventListener("scroll", handleShow);
    };
  }, []);

  return (
    <div className={`header ${show && "header_black"}`}>
      <Link to="/" style={{ textDecoration: "none" }}>
        <div className="logo">
          {/* <img src={movieLogo} alt="logo" className="movie-logo-header" /> */}
          {/* <h1 className="logo-name">ACTFLIX</h1> */}
          <img src={actflix} className="logo-new" />
        </div>
      </Link>
      <div>{props.searchBar}</div>
      <div className="sign-in">
        {localStorage.getItem("token") !== null ? (
          //user  -->logic nadya
          <Hero handleSignOut={handleSignOut} />
        ) : (
          <SignInModal
            email={email}
            setEmail={setEmail}
            password={password}
            setPassword={setPassword}
            handleLogin={handleLogin}
            handleSignUp={handleSignUp}
            hasAccount={hasAccount}
            setHasAccount={setHasAccount}
            emailError={emailError}
            passwordError={passwordError}
            setisSignedIn={props.setisSignedIn}
          />
        )}
        {/* <Link to="/watch-list">
          <div>
            <h2>Watch List</h2>
          </div>
        </Link> */}
      </div>
    </div>
  );
};

export default Header;
