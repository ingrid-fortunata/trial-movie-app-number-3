import React, { useState } from "react";
import Header from "../../Components/Layout/Header/header";
import Banner from "../../Components/Banner";
import Movies from "../../Components/Movies2";
import Footer from "../../Components/Layout/Footer/Footer";
import Search from "../../Components/Search/search";
import "../HomePage/homepage.css";

const HomePage = (props) => {
  // const [input, setInput] = useState("");

  // const handleChange = (e) => {
  //   setInput(e.target.value);
  // };
  // const handleSubmit = (e) => {
  //   e.preventDefault();
  // };
  // const clearSearchbarInput = () => {
  //   document.getElementsByClassName("search")[0].value = "";
  // };

  const Content = (props) => {
    const isSearching = props.lengthSearch > 0 ? true : false;
    return isSearching ? (
      <Search input={props.input} setSelectedMovie={props.setSelectedMovie} />
    ) : (
      <Movies setSelectedMovie={props.setSelectedMovie} />
    );
  };

  return (
    <div>
      {/* <Header
        searchBar={
          <div className="searchbar">
            <form action="" onSubmit={handleSubmit}>
              <input
                type="text"
                placeholder="Search..."
                className="search"
                value={input}
                onChange={handleChange}
              />
            </form>
          </div>
        }
      /> */}
      {(props.input.length > 0 ? false : true) ? <Banner /> : null}
      <Content
        lengthSearch={props.input.length}
        input={props.input}
        setSelectedMovie={props.setSelectedMovie}
      />
      {/* <Movies setSelectedMovie={props.setSelectedMovie} />
      <Search
        input={input}
        setSelectedMovie={props.setSelectedMovie}
        clearSearchbarInput={clearSearchbarInput}
      /> */}
      {/* <Footer /> */}
    </div>
  );
};

export default HomePage;
