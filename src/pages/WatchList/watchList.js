import React, { useState, useEffect } from "react";
import MovieCard from "../../Components/Search/movieCard-search";
import Header from "../../Components/Layout/Header/header";
import Footer from "../../Components/Layout/Footer/Footer";
import Search from "../../Components/Search/search";
import { Link } from "react-router-dom";
import "../../Components/MovieCard/movieCard-search.css";

const WatchList = (props) => {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    setMovies(props.watchList);
  }, [props.watchList]);
  // console.log(movies);

  const [input, setInput] = useState("");

  const handleChange = (e) => {
    setInput(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
  };
  if (input.length === 0) {
    return (
      <div>
        <Header
          searchBar={
            <div className="searchbar">
              <form action="" onSubmit={handleSubmit}>
                <input
                  type="text"
                  placeholder="Search..."
                  className="search"
                  value={input}
                  onChange={handleChange}
                />
              </form>
            </div>
          }
        />
        {movies.length === 0 ? (
          <div style={{ "text-align": "center" }}>
            <h2
              style={{
                color: "#e5e5e5",
                "text-align": "center",
                margin: "2.5rem ",
                "margin-bottom": "1.5rem",
              }}
            >
              It looks like there's nothing in your watch list...
            </h2>
            <Link to="/">
              <button>Add Movies!</button>
            </Link>
          </div>
        ) : (
          <div className="movie-container">
            {movies.length > 0 &&
              movies.map((movie) => <MovieCard key={movie.id} {...movie} />)}
          </div>
        )}

        <Footer />
      </div>
    );
  } else {
    return (
      <div>
        <Header
          searchBar={
            <div className="searchbar">
              <form action="" onSubmit={handleSubmit}>
                <input
                  type="text"
                  placeholder="Search..."
                  className="search"
                  value={input}
                  onChange={handleChange}
                />
              </form>
            </div>
          }
        />
        <Search input={input} />

        <Footer />
      </div>
    );
  }
};

export default WatchList;
